patchelf-wrapper.py
===================

A simple wrapper to patchelf written in Python.

This script uses [`patchelf`](https://github.com/NixOS/patchelf) to 
automatically fix dynamic library dependencies, looking for available libs in
the nix store.

It can be used to patch binaries or .so files.

It depends on `ldd`, `file` and `patchelf`.

Usage
-----
```
$ patchelf-wrapper.py FILE
```
