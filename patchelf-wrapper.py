#!/usr/bin/env python3

import subprocess
import argparse
import os
import os.path
import shutil
import sys

"""
patch-wrapper: a wrapper around patchelf to automagically look for correct dependencies in nix store.

Based on ldd, find, file, patchelf
"""


def main():
    parser = argparse.ArgumentParser(description="A wrapper to patchelf")
    parser.add_argument("file", help="File to patch")

    args = parser.parse_args()

    # dependency checks for the script
    deps = ["ldd", "find", "file", "patchelf"]
    for d in deps:
        if shutil.which(d) is None:
            sys.exit(f"{d} not found in PATH")

    # This list will contain the unresolved dependencies of the input file
    deplist = []
    file_stat = subprocess.run(
        ["file", args.file], capture_output=True, text=True
    ).stdout.split(" ")

    # get the list of missing so deps
    output = subprocess.run(["ldd", args.file], capture_output=True, text=True).stdout
    missing_deps = [
        line.split(" ")[0].strip() for line in output.split("\n") if "not found" in line
    ]
    print(f"{missing_deps}")

    so_files_in_folder = [
        os.path.basename(f) for f in os.listdir() if f.endswith(".so")
    ]
    print(f"{so_files_in_folder}")

    # find if they exist in the current folder
    if any(e in so_files_in_folder for e in missing_deps):
        print("Adding current folder")
        deplist.append(os.path.curdir)

    # find the remaining ones in the nix store
    for so in missing_deps:
        print(f"Parsing {so}")
        if so not in so_files_in_folder:
            print(f"{so} is not in the current folder")
            store_paths = subprocess.run(
                ["find", "/nix/store", "-name", f"*{so}"],
                capture_output=True,
                text=True,
            ).stdout.split("\n")
            print(f"Found in store: {store_paths}")
            for el in store_paths:
                final = os.path.realpath(el, strict=True)
                # check ELF32/64 format (necessary for libstdc++.so for example)
                so_stat = subprocess.run(
                    ["file", final], capture_output=True, text=True
                ).stdout.split(" ")
                # field 1 is ELF, field 2 is 32/64-bit
                if file_stat[1:3] == so_stat[1:3]:
                    # for each missing dep, extract the base path
                    p = os.path.dirname(el)
                    print(f"Found {so}")
                    print(f"Appending {p}")
                    deplist.append(p)
                    break
            else:
                sys.exit(f"No .so found for {so}")

    # call patchelf with the list of paths
    print(f"Deplist: {deplist}")
    print("Patching")
    cmd = ["patchelf"]
    if file_stat[4] == "executable":
        cmd.append("--set-interpreter")
        cmd.append('"$(cat $NIX_CC/nix-support/dynamic-linker)"')

    sp = subprocess.run(
        cmd
        + [
            "--set-rpath",
            ":".join(deplist),
            args.file,
        ]
    )
    print("Ran: {}".format(" ".join(sp.args)))
    sp.check_returncode()
    print("Success")


if __name__ == "__main__":
    main()
